FROM golang:1.20.1-alpine3.16 as builder

WORKDIR /app
RUN export GOPROXY=https://goproxy.io,direct

COPY go.mod ./
COPY go.sum ./

RUN go mod download
COPY . .

RUN go build -v -o /application

FROM alpine:3.16
COPY --from=builder /application /application
COPY --from=builder /app/.env /app/
RUN export $(cat app/.env | xargs)
EXPOSE 8082

CMD [ "/application"]