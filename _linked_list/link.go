package main

import (
	"fmt"
)

type LinkedNode struct {
	Val  int
	Next *LinkedNode
}

func addTwoNumbers(list1 *LinkedNode, list2 *LinkedNode) *LinkedNode {
	dummy := &LinkedNode{}
	curr := dummy
	carry := 0

	for list1 != nil || list2 != nil || carry != 0 {
		sum := carry

		if list1 != nil {
			sum += list1.Val
			list1 = list1.Next
		}

		if list2 != nil {
			sum += list2.Val
			list2 = list2.Next
		}

		curr.Next = &LinkedNode{Val: sum % 10}
		curr = curr.Next
		carry = sum / 10
	}

	return dummy.Next
}

func main() {
	l1 := &LinkedNode{Val: 6, Next: &LinkedNode{Val: 4, Next: &LinkedNode{Val: 3}}}
	l2 := &LinkedNode{Val: 7, Next: &LinkedNode{Val: 8, Next: &LinkedNode{Val: 3}}}

	result := addTwoNumbers(l1, l2)

	// Print the result
	for result != nil {
		fmt.Printf("%d -> ", result.Val)
		result = result.Next
	}
}
