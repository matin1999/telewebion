package controllers

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"math/rand"
	"net/http"
	"time"
)

func (h handler) GetLotteryChance(ctx *gin.Context) {
	//validation user
	uuid := ctx.PostForm("UUID")
	if uuid == "" {
		//http.Error(w, "user_id is required", http.StatusBadRequest)
		ctx.JSON(http.StatusBadRequest, "uuid is required")
		return
	}
	uuidString := fmt.Sprintf("%v", uuid)
	attempts, err := h.getAttempts(uuidString)

	if attempts >= 3 {
		ctx.JSON(http.StatusTooManyRequests, "reach your limit")
		return
	}
	err = h.incrementAttempts(uuidString)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, "internal error")
		return
	}

	func() {
		prize := doLottery()
		log.Print("response")
		log.Print(prize)
		ctx.JSON(http.StatusOK, gin.H{"your prize": prize})
		return
	}()

}

func (h handler) getAttempts(uuid string) (int, error) {

	ctx := context.TODO()
	log.Print(h)

	attempts, err := h.DB.Get(ctx, uuid).Int()
	if err != nil {
		return 0, err
	}
	return attempts, nil
}

func (h handler) incrementAttempts(uuid string) error {
	ctx := context.TODO()
	_, err := h.DB.Incr(ctx, uuid).Result()
	h.DB.ExpireAt(ctx, uuid, time.Now().Add(24*time.Hour).Truncate(24*time.Hour))
	return err

}

func doLottery() string {

	prizes := []string{"A", "B", "C", "D", "E"}
	weights := []float64{0.1, 0.3, 0.2, 0.15, 0.25}
	r := rand.Float64()
	log.Print(r)
	for i, w := range weights {

		log.Print(w)
		if r < w {
			log.Print("function if")
			log.Print(prizes[i])
			return prizes[i]
		}
		r -= w
	}
	log.Print("de")
	return "E"
}
