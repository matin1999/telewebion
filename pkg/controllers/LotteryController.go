package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/redis/go-redis/v9"
)

type handler struct {
	DB *redis.Client
}

func RegisterRoutes(router *gin.Engine, db *redis.Client, groupPath string) {
	h := &handler{
		DB: db,
	}
	routes := router.Group(groupPath)

	routes.POST("/", h.GetLotteryChance)

}
