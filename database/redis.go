package database

import (
	"context"
	"github.com/redis/go-redis/v9"
	"log"
)

func Init(address string) *redis.Client {
	Ctx := context.TODO()

	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: "",
		DB:       1,
	})
	log.Print("aaaaaaaa")
	log.Print(client.Ping(Ctx))
	if err := client.Ping(Ctx).Err(); err != nil {
		log.Print(err)
		return nil
	}
	return client
}
