package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"matin.com/telewebion/database"
	"matin.com/telewebion/pkg/controllers"
)

func main() {
	dbHandler := database.Init("redis:6379")

	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(gin.Recovery())
	router.Use(gin.Logger())
	controllers.RegisterRoutes(router, dbHandler, "/api/v1/lottery")
	err := router.Run("0.0.0.0:8082")
	if err != nil {
		log.Fatalln(err)

	}
}
